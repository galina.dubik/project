let currentIndex = 0;
const buttonLeft = document.querySelector('.testimonials-button.button-left');
const buttonRight = document.querySelector('.testimonials-button.button-right');
const indicators = document.querySelectorAll('.indication-item');


function updateIndicators(index) {
  indicators.forEach((indicator, i) => {

    if (i === index) {
      indicator.style.backgroundColor = '#003366';
      indicator.style.width = '20px';
    } else {
      indicator.style.backgroundColor = '#c2d1e0';
      indicator.style.width = '4px';
    }
  });
}


buttonRight.addEventListener('click', () => {
    currentIndex = (currentIndex + 1) % indicators.length;
    updateIndicators(currentIndex);
    buttonRight.style.backgroundColor = '#003366';
});

buttonLeft.addEventListener('click', () => {
    currentIndex = (currentIndex - 1 + indicators.length) % indicators.length;
    updateIndicators(currentIndex);
    buttonLeft.style.backgroundColor = '#003366';
});